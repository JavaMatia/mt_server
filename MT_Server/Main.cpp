#include "WSAInitializer.h"
#include "Server.hpp"
#include <iostream>

int main(void)
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve(8200); //serve on port 8200
	}
	catch (const std::exception & e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}