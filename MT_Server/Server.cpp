#include <iostream>
#include "Server.hpp"
#include "Server_Exception.h"

bool canSendMessage = true;

std::mutex usersLock; // use it everytime we access this shared resource
std::mutex fileLock; //use it everytime we access this shared resource
std::mutex messageLock;

std::condition_variable cv;
std::mutex cvMutex;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
		//change the state of isRunning so the workerThread will be killed
		this->_isRunning = false;
		cv.notify_all();
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)& sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	this->_isRunning = true;
	//server is running now. start the worker thread
	std::thread workerThread(&Server::updateDocumentWorkerThread, this);
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread t(&Server::attemptLogin, this, client_socket);
	t.detach();
}


/*
The function adds a successfully connected user to the queue
Input: the client specific socket
Output: none
*/
void Server::attemptLogin(SOCKET clientSocket)
{
	int messageCode = 0; //we expect 200
	std::pair<std::string, SOCKET> newPair;
	try
	{
		std::cout << "Accepting a connection..." << std::endl;

		messageCode = Helper::getMessageTypeCode(clientSocket);
		if (LOGIN_MESSAGE == messageCode)
		{
			//it is a login message, we can log in
			int nameLength = Helper::getIntPartFromSocket(clientSocket, 2);
			newPair.first = Helper::getStringPartFromSocket(clientSocket, nameLength);
			newPair.second = clientSocket;

			std::unique_lock<std::mutex> locker(usersLock);
			this->_usersInQueue.push_back(newPair);
			locker.unlock();
			std::cout << newPair.first + " Has connected! Hooray!" << std::endl;
		}

	}
	catch (const std::exception & e)
	{
		closesocket(clientSocket);
		std::cout << "Error occured: " << e.what() << std::endl;
	}

	//Handle the user in other function
	sendCurrentPositionInQueue(newPair.second, newPair.first);
	handleClients(newPair);
}

void Server::handleClients(std::pair<std::string, SOCKET> user)
{
	int messageCode = 0; 
	bool isConnected = true;

	while (this->_isRunning && isConnected)
	{
		messageCode = Helper::getMessageTypeCode(user.second);
		switch (messageCode)
		{
		case UPDATE_DOCUMENT:
		{
			updateMessageQueue(user.second);
			break;
		}
		case UPDATE_FINISHED:
			handleFinishEditing();
			break;
		case EXIT:
			handleDisconection(user);
			closesocket(user.second);
			isConnected = false;
			break;
		default:
			break;
		}
		//std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

/*
The function sends the position of the user in the queue to the client. 1- the user can edit
Input: (string) the username we check in the list
Output: None
*/
void Server::sendCurrentPositionInQueue(SOCKET clientSocket, std::string username)
{
	std::deque<std::pair<std::string, SOCKET>>::iterator it;

	std::string contents;
	int usernameSize = (int) username.size();

	std::string nextUsernameName = "";
	int nextUsernameSize = 0;

	int fileLen;
	int position = 1;

	std::string finalMessage;

	//find current and next positions

	std::unique_lock<std::mutex> locker(usersLock);
	for (it = this->_usersInQueue.begin(); it != this->_usersInQueue.end(); it++)
	{
		if (it->first != username) // if it is not the username, so his place in the queue is further
		{
			position++;
		}
		else
		{
			it++;
			if (it!=this->_usersInQueue.end())
			{
				nextUsernameName = it->first;
				nextUsernameSize = (int) nextUsernameName.size();
			}
			break; //we found his position
		}
	}
	locker.unlock();
	//now that we have the position, send the message

	//handles the file part and locks it
	std::unique_lock<std::mutex> filerLocker(fileLock);
	Helper::openFileRead(this->_document, FILE_NAME); // neat function to open / create file HAS TO BE READ ONLY
	contents.assign((std::istreambuf_iterator<char>(this->_document)), (std::istreambuf_iterator<char>())); //should read the entire file
	this->_document.close();
	filerLocker.unlock();

	fileLen = (int) contents.size();
	
	//the payload we send to the client
	finalMessage =
		SUBMUIT_CLIENT_POSITION +
		Helper::getPaddedNumber(fileLen, 5) +
		contents +
		Helper::getPaddedNumber(usernameSize, 2) +
		username +
		Helper::getPaddedNumber(nextUsernameSize, 2) +
		nextUsernameName +
		std::to_string(position);
	std::cout << "Queue position update: First user: " + username + " next username: " + nextUsernameName << std::endl;
	Helper::sendData(clientSocket, finalMessage);
}

/*
The function pushes the messages to the message queue and notifies the worker thread
Input: client socket
Output: None
*/
void Server::updateMessageQueue(SOCKET clientSocket)
{
	int fileSize = Helper::getIntPartFromSocket(clientSocket, 5); // the size of the file
	std::string content = Helper::getStringPartFromSocket(clientSocket, fileSize);
	std::cout << "Message from client: " + content << std::endl;

	std::unique_lock<std::mutex> messageUniqueLock(messageLock);
	this->_messageQueue.push(content);
	messageUniqueLock.unlock();
	//notify the worker thread to read from the message queue
	cv.notify_all();
}

/*
Only the worker thread can access this function. the thread waits to be notified and then it writes everything in the queue to the file
Input: None
Output: None
*/
void Server::updateDocumentWorkerThread()
{
	std::unique_lock<std::mutex> locker(cvMutex);
	while (this->_isRunning)
	{
		cv.wait(locker); //while the server is running, wait to be notified. then - 

		std::unique_lock<std::mutex> uniqueFileLock(fileLock);
		std::unique_lock<std::mutex> messageUniqueLock(messageLock);
		Helper::openFileWrite(this->_document, FILE_NAME); // open with write permission
		std::cout << "[WORKER THREAD] Updating document" << std::endl;
		while (!this->_messageQueue.empty())
		{
			std::string message = this->_messageQueue.back();
			this->_messageQueue.pop();
			this->_document << message;

		}
		std::cout << "[WORkER_THREAD] Done updating document" << std::endl;
		this->_document.close();
		uniqueFileLock.unlock();
		messageUniqueLock.unlock();
		//send 101 to all users. NOTE: i have to do this from here otherwise i can expect undefined behavior
		std::unique_lock<std::mutex> locker(usersLock);
		for (auto it = this->_usersInQueue.begin(); it != this->_usersInQueue.end(); it++) // SEND 101 TO ALL
		{
			locker.unlock();
			sendCurrentPositionInQueue(it->second, it->first);
			locker.lock();
		}
		locker.unlock();
	}
}

/*
The function handles 207 - it moves the first user to the back, update the message queue, and send update to the client
Input: None
Output: None
*/
void Server::handleFinishEditing()
{
	//move the user to the back (and lock before)
	std::unique_lock<std::mutex> userUniqueLock(usersLock);
	std::pair<std::string, SOCKET> user = this->_usersInQueue.front();
	//update the message queue
	this->updateMessageQueue(user.second);

	//move the client to the end of the queue
	this->_usersInQueue.pop_front();
	this->_usersInQueue.push_back(user);
}
/*
Handles the disconnection of a user
*/
void Server::handleDisconection(std::pair<std::string, SOCKET> user)
{
	std::unique_lock<std::mutex> userUniqueLock(usersLock);

	std::deque<std::pair<std::string, SOCKET>>::iterator it = this->_usersInQueue.begin();
	for (it; it != this->_usersInQueue.end(); it++)
	{
		if (it->first == user.first)
		{
			std::cout << it->first << " Disconnected" << std::endl;
			this->_usersInQueue.erase(it);

			break;
		}
	}

	if (!this->_usersInQueue.empty())
	{
		for (auto it = this->_usersInQueue.begin(); it != this->_usersInQueue.end(); it++) // SEND 101 TO ALL
		{
			userUniqueLock.unlock();
			sendCurrentPositionInQueue(it->second, it->first);
			userUniqueLock.lock();
		}
		userUniqueLock.unlock();
	}
	else
	{
		userUniqueLock.unlock();
	}
}




