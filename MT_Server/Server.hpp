#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <queue>
#include <thread>
#include <mutex>
#include <fstream>
#include <condition_variable>
#include "Helper.h"

#define RESPONSE_BUFFER_SIZE 20
#define LOGIN_MESSAGE 200
#define SUBMUIT_CLIENT_POSITION "101"
#define UPDATE_DOCUMENT 204
#define UPDATE_FINISHED 207
#define EXIT 208
#define FILE_NAME "super_secret.txt"
class Server
{
public:
	Server();
	~Server();
	void serve(int port);


private:
	void accept();
	void attemptLogin(SOCKET clientSocket);
	void handleClients(std::pair<std::string, SOCKET> user);
	void sendCurrentPositionInQueue(SOCKET clientSocket, std::string username);
	void updateMessageQueue(SOCKET clientSocket);
	void updateDocumentWorkerThread();
	void handleFinishEditing();
	void handleDisconection(std::pair<std::string, SOCKET> user);

	//Class fields
	bool _isRunning = false;
	std::fstream _document;
	std::queue<std::string> _messageQueue;
	std::deque<std::pair<std::string, SOCKET>> _usersInQueue;
	SOCKET _serverSocket;
};
