#pragma once
#include <exception>
#include <iostream>
#include <Windows.h>

class Server_Exception : public std::exception
{
public:
	const char* what() const;
	Server_Exception();
	~Server_Exception();
};

